package net.jaardvark.jcr.util;

import java.util.Comparator;

import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

public class ValueComparator implements Comparator<Value> {

	@Override
	public int compare(Value o1, Value o2) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	public static boolean equals(Value o1, Value o2) throws RepositoryException {
		if (o2==null)
			return false;
		if (o1.getType()==o2.getType())
			return o1.getString().equals(o2.getString());
		return false;
	}



	public static boolean gt(Value o1, Value o2) throws ValueFormatException, RepositoryException {
		if (o2==null)
			return true;
		if (o1.getType()==o2.getType() && o1.getType()==PropertyType.DATE){
			// compare dates
			return o1.getDate().compareTo(o2.getDate()) > 0;
		}
		if ( (o1.getType()==PropertyType.DOUBLE || o1.getType()==PropertyType.LONG)
			&& (o2.getType()==PropertyType.DOUBLE || o2.getType()==PropertyType.LONG)){
			// compare numbers
			return o1.getDouble() > o2.getDouble();
		}
		return false;
	}



	public static boolean gte(Value o1, Value o2) throws ValueFormatException, RepositoryException {
		if (o2==null)
			return true;
		if (o1.getType()==o2.getType() && o1.getType()==PropertyType.DATE){
			// compare dates
			return o1.getDate().compareTo(o2.getDate()) >= 0;
		}
		if ( (o1.getType()==PropertyType.DOUBLE || o1.getType()==PropertyType.LONG)
			&& (o2.getType()==PropertyType.DOUBLE || o2.getType()==PropertyType.LONG)){
			// compare numbers
			return o1.getDouble() >= o2.getDouble();
		}
		return false;
	}



	public static boolean lt(Value o1, Value o2) throws ValueFormatException, RepositoryException {
		if (o2==null)
			return false;
		if (o1.getType()==o2.getType() && o1.getType()==PropertyType.DATE){
			// compare dates
			return o1.getDate().compareTo(o2.getDate()) < 0;
		}
		if ( (o1.getType()==PropertyType.DOUBLE || o1.getType()==PropertyType.LONG)
			&& (o2.getType()==PropertyType.DOUBLE || o2.getType()==PropertyType.LONG)){
			// compare numbers
			return o1.getDouble() < o2.getDouble();
		}
		return false;
	}



	public static boolean lte(Value o1, Value o2) throws ValueFormatException, RepositoryException {
		if (o2==null)
			return false;
		if (o1.getType()==o2.getType() && o1.getType()==PropertyType.DATE){
			// compare dates
			return o1.getDate().compareTo(o2.getDate()) <= 0;
		}
		if ( (o1.getType()==PropertyType.DOUBLE || o1.getType()==PropertyType.LONG)
			&& (o2.getType()==PropertyType.DOUBLE || o2.getType()==PropertyType.LONG)){
			// compare numbers
			return o1.getDouble() <= o2.getDouble();
		}
		return false;
	}
	

}
